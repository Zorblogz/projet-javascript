import React from 'react';
import ReactDOM from 'react-dom';
import { Formes, Ligne, Cercle, Rectangle, Ancre, Forme } from "./formes.js";

var formes = new Formes();
var selected_shape;

class Page extends React.Component{
	constructor(props){
		super(props);
	}
	render(){
		return(
			<div className="page">
				<ShapeCreator> </ShapeCreator>
				<div className="fond">
					<SVG></SVG>
				</div>	
			</div>
		)
	}
}

class ShapeCreator extends React.Component{
	constructor(props){
		super(props);
		this.state={forme_type: "", show: false, current_type: ""}
		this.createShape=this.createShape.bind(this)
		this.showMenu=this.showMenu.bind(this)
	}
	createShape(e){
		this.setState({forme_type: e.target.id})
	}
	showMenu(e){
		if(selected_shape != null){
			selected_shape.cacherAncres();
			selected_shape = null;
		}
		selected_shape = formes.liste.get(e.target.id);
		selected_shape.dessinerAncres();
		this.setState({forme_type: "", show: true, current_type: e.target.id})
	}
	render(){
		if(this.state.forme_type=="add_rect"){
			var forme = new Rectangle();
			formes.add(forme);
		}
		else if(this.state.forme_type=="add_circle"){
			var forme = new Cercle();
			formes.add(forme);
		}
		else if(this.state.forme_type=="add_line"){
			var forme = new Ligne();
			formes.add(forme);
		}

		for(let item of formes.liste.values()){
			document.querySelector('#'+item.id).addEventListener("click", this.showMenu, false);
		}
		
		return(
			<div id="nav">
				<h3>Ajouter Forme: </h3>
				<ShapeInput type="button" id="add_rect" value="Ajouter Rectangle" action={this.createShape}></ShapeInput>
				<ShapeInput type="button" id="add_circle" value="Ajouter Cercle" action={this.createShape}></ShapeInput>
				<ShapeInput type="button" id="add_line" value="Ajouter Ligne" action={this.createShape}></ShapeInput>
				<ShapeEditor show={this.state.show} type={this.state.current_type}></ShapeEditor>
			</div>
		)
	}
}

class SVG extends React.Component{
	constructor(props){
		super(props);
	}
	render(){
		return(
			<svg id="feuille" width="70%" height="70%">
			</svg>
		)
	}
}

class ShapeInput extends React.Component{
	constructor(props){
		super(props);
		console.log(this)
	}
	render(){
		return(
			<input type={this.props.type} id={this.props.id} value={this.props.value} onClick={this.props.action}/>
		)
	}
}

class ShapeEditor extends React.Component{
	constructor(props){
		super(props);	
	}
	render(){
		console.log(this.props.type.replace(/[^A-Za-z]+/g, ''))
		if(this.props.show==true){
			if(this.props.type.replace(/[^A-Za-z]+/g, '')=="rect"){
				return(
					<div>
						<RectEditor></RectEditor>
					</div>
			)}
			else if(this.props.type.replace(/[^A-Za-z]+/g, '')=="circle"){
				return(
					<div>
						<CircleEditor></CircleEditor>
					</div>
				)
			}
			else if(this.props.type.replace(/[^A-Za-z]+/g, '')=="line"){
				return(
					<div>
						<LineEditor></LineEditor>
					</div>
				)
			}
		}
		return(null)
	}
}

class RectEditor extends React.Component{
	constructor(props){
		super(props);
	}
	colorSet(){
		let color = document.querySelector("#fill_color").value;
		selected_shape.color=color;
	}
	strokeColorSet(){
		let color = document.querySelector("#stroke_color").value;
		console.log(document.querySelector("#stroke_color").value);
		selected_shape.stroke_color=color;
	}
	strokeSet(){
		let stroke = document.querySelector("#stroke").value;
		selected_shape.stroke=stroke
		console.log(formes)
	}
	render(){
		return(
		<div id="forme_menu">
		<h3>Modifier la forme: </h3>
			<h4> Couleur de la forme : </h4>
			<input id="fill_color" type="text" placeholder={selected_shape.color}/>
			<input type="submit" onClick={this.colorSet}/>
			<h4> Couleur de la bordure : </h4> 
			<input id="stroke_color" type="text" placeholder={selected_shape.stroke_color} /> 
			<input type="submit" onClick={this.strokeColorSet}/>
			<h4> Epaisseur de la bordure : </h4>
			<input id="stroke" type="text" placeholder={selected_shape.stroke}/>
			<input type="submit" onClick={this.strokeSet}/>
		</div>
		)
	}
}

class CircleEditor extends React.Component{
	constructor(props){
		super(props);
	}
	colorSet(){
		let color = document.querySelector("#fill_color").value;
		selected_shape.color=color;
	}
	strokeColorSet(){
		let color = document.querySelector("#stroke_color").value;
		console.log(document.querySelector("#stroke_color").value);
		selected_shape.stroke_color=color;
	}
	strokeSet(){
		let stroke = document.querySelector("#stroke").value;
		selected_shape.stroke=stroke
		console.log(formes)
	}
	render(){
		return(
		<div id="forme_menu">
		<h3>Modifier la forme: </h3>
			<h4> Couleur de la forme : </h4>
			<input id="fill_color" type="text" placeholder={selected_shape.color}/>
			<input type="submit" onClick={this.colorSet}/>
			<h4> Couleur de la bordure : </h4> 
			<input id="stroke_color" type="text" placeholder={selected_shape.stroke_color} /> 
			<input type="submit" onClick={this.strokeColorSet}/>
			<h4> Epaisseur de la bordure : </h4>
			<input id="stroke" type="text" placeholder={selected_shape.stroke}/>
			<input type="submit" onClick={this.strokeSet}/>
		</div>
		)
	}
}

class LineEditor extends React.Component{
	constructor(props){
		super(props);
	}
	colorSet(){
		let color = document.querySelector("#fill_color").value;
		selected_shape.stroke_color=color;
	}
	strokeSet(){
		let stroke = document.querySelector("#stroke").value;
		selected_shape.stroke=stroke
		console.log(formes)
	}
	render(){
		return(
		<div id="forme_menu">
		<h3>Modifier la forme: </h3>
			<h4> Couleur : </h4> 
			<input id="fill_color" type="text" placeholder={selected_shape.stroke_color} /> 
			<input type="submit" onClick={this.colorSet}/>
			<h4> Epaisseur de la bordure : </h4>
			<input id="stroke" type="text" placeholder={selected_shape.stroke}/>
			<input type="submit" onClick={this.strokeSet}/>
		</div>
		)
	}
}

ReactDOM.render(React.createElement(Page),document.getElementById('main'));
//ReactDOM.render(React.createElement(Menu),document.getElementById('page'));