(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["myApp"] = factory();
	else
		root["myApp"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar svg = \"http://www.w3.org/2000/svg\";\n\nvar idunique = function () {\n\tvar id = 0;\n\treturn function (se) {\n\t\treturn id++;\n\t};\n}();\n\nvar Ancre = function () {\n\tfunction Ancre(x, y, _id) {\n\t\t_classCallCheck(this, Ancre);\n\n\t\tconsole.log(x + \" \" + y);\n\t\tthis._x = x;\n\t\tthis._y = y;\n\t\tthis._taille = 10;\n\t\tthis.id = _id;\n\t\tthis.dessin = document.createElementNS(svg, \"rect\");\n\t\tthis.dessin.setAttribute(\"id\", this.id);\n\t\tthis.dessin.setAttribute(\"width\", this.taille);\n\t\tthis.dessin.setAttribute(\"height\", this.taille);\n\t\tthis.dessin.setAttribute(\"x\", this.x);\n\t\tthis.dessin.setAttribute(\"y\", this.y);\n\t\tthis.dessin.setAttribute(\"fill\", \"black\");\n\t}\n\n\t_createClass(Ancre, [{\n\t\tkey: \"dessiner\",\n\t\tvalue: function dessiner() {\n\t\t\tfeuille.appendChild(this.dessin);\n\t\t}\n\t}, {\n\t\tkey: \"cacher\",\n\t\tvalue: function cacher() {\n\t\t\tfeuille.removeChild(this.dessin);\n\t\t}\n\t}, {\n\t\tkey: \"move\",\n\t\tvalue: function move(movementX, movementY) {\n\t\t\tthis.x += movementX;\n\t\t\tthis.y += movementY;\n\t\t}\n\t}, {\n\t\tkey: \"x\",\n\t\tget: function get() {\n\t\t\treturn this._x;\n\t\t},\n\t\tset: function set(value) {\n\t\t\tthis._x = value;\n\t\t\tthis.dessin.setAttribute(\"x\", this.x);\n\t\t}\n\t}, {\n\t\tkey: \"y\",\n\t\tget: function get() {\n\t\t\treturn this._y;\n\t\t},\n\t\tset: function set(value) {\n\t\t\tthis._y = value;\n\t\t\tthis.dessin.setAttribute(\"y\", this.y);\n\t\t}\n\t}, {\n\t\tkey: \"taille\",\n\t\tget: function get() {\n\t\t\treturn this._taille;\n\t\t}\n\t}]);\n\n\treturn Ancre;\n}();\n\n;\n\nvar Forme = function () {\n\tfunction Forme(x, y, forme) {\n\t\t_classCallCheck(this, Forme);\n\n\t\tthis._id = forme + idunique();\n\n\t\tthis.proxy = new Map();\n\t\tthis.proxy.set(\"move_proxy\", new Ancre(x, y, \"move\"));\n\n\t\tthis._color = \"transparent\";\n\t\tthis._stroke_color = \"black\";\n\t\tthis._stroke = 3;\n\n\t\tthis.dessin = document.createElementNS(svg, forme);\n\t\tthis.dessin.setAttribute(\"id\", this.id);\n\t\tthis.dessin.setAttribute(\"fill\", this._color);\n\t\tthis.dessin.setAttribute(\"stroke\", this._stroke_color);\n\t\tthis.dessin.setAttribute(\"stroke-width\", this._stroke);\n\t}\n\n\t_createClass(Forme, [{\n\t\tkey: \"dessinerAncres\",\n\t\tvalue: function dessinerAncres() {\n\t\t\tthis.proxy.get(\"move_proxy\").dessiner();\n\t\t\tthis.proxy.get(\"resize_proxy\").dessiner();\n\t\t}\n\t}, {\n\t\tkey: \"cacherAncres\",\n\t\tvalue: function cacherAncres() {\n\t\t\tthis.proxy.get(\"move_proxy\").cacher();\n\t\t\tthis.proxy.get(\"resize_proxy\").cacher();\n\t\t}\n\t}, {\n\t\tkey: \"supprimer\",\n\t\tvalue: function supprimer() {\n\t\t\tthis.cacherAncres();\n\t\t\tfeuille.removeChild(this.dessin);\n\t\t\tdelete this.proxy.get(\"move_proxy\");\n\t\t\tdelete this.proxy.get(\"resize_proxy\");\n\t\t\tconsole.log(this);\n\t\t}\n\t}, {\n\t\tkey: \"id\",\n\t\tget: function get() {\n\t\t\treturn this._id;\n\t\t}\n\t}, {\n\t\tkey: \"color\",\n\t\tget: function get() {\n\t\t\treturn this._color;\n\t\t},\n\t\tset: function set(value) {\n\t\t\tthis._color = value;\n\t\t\tthis.dessin.setAttribute(\"fill\", this._color);\n\t\t}\n\t}, {\n\t\tkey: \"stroke_color\",\n\t\tget: function get() {\n\t\t\treturn this._stroke_color;\n\t\t},\n\t\tset: function set(value) {\n\t\t\tthis._stroke_color = value;\n\t\t\tthis.dessin.setAttribute(\"stroke\", this._stroke_color);\n\t\t}\n\t}, {\n\t\tkey: \"stroke\",\n\t\tget: function get() {\n\t\t\treturn this._stroke;\n\t\t},\n\t\tset: function set(value) {\n\t\t\tthis._stroke = value;\n\t\t\tthis.dessin.setAttribute(\"stroke-width\", this._stroke);\n\t\t}\n\t}]);\n\n\treturn Forme;\n}();\n\n;\n\nvar Rectangle = function (_Forme) {\n\t_inherits(Rectangle, _Forme);\n\n\tfunction Rectangle() {\n\t\tvar _height = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 75;\n\n\t\tvar _width = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 100;\n\n\t\tvar x = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 30;\n\t\tvar y = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 50;\n\n\t\t_classCallCheck(this, Rectangle);\n\n\t\tvar _this = _possibleConstructorReturn(this, (Rectangle.__proto__ || Object.getPrototypeOf(Rectangle)).call(this, x, y, \"rect\"));\n\n\t\t_this.height = _height;\n\t\t_this.width = _width;\n\n\t\t_this.proxy.set(\"resize_proxy\", new Ancre(x + _this.width, y + _this.height, \"resize\"));\n\t\t_this.dessin.setAttribute(\"x\", _this.proxy.get(\"move_proxy\").x + _this.proxy.get(\"move_proxy\").taille / 2);\n\t\t_this.dessin.setAttribute(\"y\", _this.proxy.get(\"move_proxy\").y + _this.proxy.get(\"move_proxy\").taille / 2);\n\t\t_this.dessin.setAttribute(\"width\", _this.width);\n\t\t_this.dessin.setAttribute(\"height\", _this.height);\n\t\tfeuille.appendChild(_this.dessin);\n\t\treturn _this;\n\t}\n\n\t_createClass(Rectangle, [{\n\t\tkey: \"resize\",\n\t\tvalue: function resize(movementX, movementY) {\n\t\t\tif (this.height > 5) {\n\t\t\t\tthis.height += movementY;\n\t\t\t\tthis.proxy.get(\"resize_proxy\").y += movementY;\n\t\t\t\tthis.dessin.setAttribute(\"height\", this.height);\n\t\t\t} else if (this.height <= 5) {\n\t\t\t\tthis.height += 1;\n\t\t\t\tthis.proxy.get(\"resize_proxy\").y += 1;\n\t\t\t}\n\n\t\t\tif (this.width > 5) {\n\t\t\t\tthis.width += movementX;\n\t\t\t\tthis.proxy.get(\"resize_proxy\").x += movementX;\n\t\t\t\tthis.dessin.setAttribute(\"width\", this.width);\n\t\t\t} else if (this.width <= 5) {\n\t\t\t\tthis.width += 1;\n\t\t\t\tthis.proxy.get(\"resize_proxy\").x += 1;\n\t\t\t}\n\t\t}\n\t}, {\n\t\tkey: \"move\",\n\t\tvalue: function move(movementX, movementY) {\n\t\t\tthis.proxy.get(\"move_proxy\").move(movementX, movementY);\n\t\t\tthis.proxy.get(\"resize_proxy\").move(movementX, movementY);\n\n\t\t\tthis.dessin.setAttribute(\"x\", this.proxy.get(\"move_proxy\").x + this.proxy.get(\"move_proxy\").taille / 2);\n\t\t\tthis.dessin.setAttribute(\"y\", this.proxy.get(\"move_proxy\").y + this.proxy.get(\"move_proxy\").taille / 2);\n\t\t}\n\t}]);\n\n\treturn Rectangle;\n}(Forme);\n\n;\n\nvar Cercle = function (_Forme2) {\n\t_inherits(Cercle, _Forme2);\n\n\tfunction Cercle() {\n\t\tvar _rayon = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 50;\n\n\t\tvar x = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 30;\n\t\tvar y = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 50;\n\n\t\t_classCallCheck(this, Cercle);\n\n\t\tvar _this2 = _possibleConstructorReturn(this, (Cercle.__proto__ || Object.getPrototypeOf(Cercle)).call(this, x, y, \"circle\"));\n\n\t\t_this2.rayon = _rayon;\n\t\t_this2.proxy.set(\"resize_proxy\", new Ancre(x + 2 * _this2.rayon + 15, y + 2 * _this2.rayon + 15, \"resize\"));\n\t\t_this2.dessin.setAttribute(\"cx\", _this2.proxy.get(\"move_proxy\").x + _this2.rayon + 15);\n\t\t_this2.dessin.setAttribute(\"cy\", _this2.proxy.get(\"move_proxy\").y + _this2.rayon + 15);\n\t\t_this2.dessin.setAttribute(\"r\", _this2.rayon);\n\t\tfeuille.appendChild(_this2.dessin);\n\t\treturn _this2;\n\t}\n\n\t_createClass(Cercle, [{\n\t\tkey: \"move\",\n\t\tvalue: function move(movementX, movementY) {\n\t\t\tthis.proxy.get(\"move_proxy\").move(movementX, movementY);\n\t\t\tthis.proxy.get(\"resize_proxy\").move(movementX, movementY);\n\n\t\t\tthis.dessin.setAttribute(\"cx\", this.proxy.get(\"move_proxy\").x + this.rayon + 15);\n\t\t\tthis.dessin.setAttribute(\"cy\", this.proxy.get(\"move_proxy\").y + this.rayon + 15);\n\t\t}\n\t}, {\n\t\tkey: \"resize\",\n\t\tvalue: function resize(movementX, movementY) {\n\t\t\tif (this.rayon > 1) {\n\t\t\t\tthis.proxy.get(\"resize_proxy\").move(movementX, movementY);\n\t\t\t\tvar deplacement = Math.sqrt(Math.pow(movementX, 2) + Math.pow(movementY, 2));\n\t\t\t\tif (movementX > 0 || movementY > 0) {\n\t\t\t\t\tthis.rayon += deplacement;\n\t\t\t\t} else {\n\t\t\t\t\tthis.rayon -= deplacement;\n\t\t\t\t}\n\t\t\t\tthis.dessin.setAttribute(\"r\", this.rayon);\n\t\t\t} else if (this.rayon <= 1) {\n\t\t\t\tthis.rayon += 1;\n\t\t\t}\n\t\t}\n\t}]);\n\n\treturn Cercle;\n}(Forme);\n\nvar Ligne = function (_Forme3) {\n\t_inherits(Ligne, _Forme3);\n\n\tfunction Ligne() {\n\t\tvar x1 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 50;\n\n\t\tvar _x2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 150;\n\n\t\tvar y1 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 60;\n\n\t\tvar _y2 = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 100;\n\n\t\t_classCallCheck(this, Ligne);\n\n\t\tvar _this3 = _possibleConstructorReturn(this, (Ligne.__proto__ || Object.getPrototypeOf(Ligne)).call(this, x1, y1, \"line\"));\n\n\t\t_this3.x2 = _x2;\n\t\t_this3.y2 = _y2;\n\t\t_this3.proxy.set(\"resize_proxy\", new Ancre(_this3.x2, _this3.y2, \"resize\"));\n\t\t_this3.dessin.setAttribute(\"x1\", _this3.proxy.get(\"move_proxy\").x + _this3.proxy.get(\"move_proxy\").taille / 2);\n\t\t_this3.dessin.setAttribute(\"y1\", _this3.proxy.get(\"move_proxy\").y + _this3.proxy.get(\"move_proxy\").taille / 2);\n\t\t_this3.dessin.setAttribute(\"x2\", _this3.proxy.get(\"resize_proxy\").x + _this3.proxy.get(\"resize_proxy\").taille / 2);\n\t\t_this3.dessin.setAttribute(\"y2\", _this3.proxy.get(\"resize_proxy\").y + _this3.proxy.get(\"resize_proxy\").taille / 2);\n\t\tfeuille.appendChild(_this3.dessin);\n\t\treturn _this3;\n\t}\n\n\t_createClass(Ligne, [{\n\t\tkey: \"move\",\n\t\tvalue: function move(movementX, movementY) {\n\t\t\tthis.proxy.get(\"move_proxy\").move(movementX, movementY);\n\t\t\tthis.proxy.get(\"resize_proxy\").move(movementX, movementY);\n\n\t\t\tthis.dessin.setAttribute(\"x1\", this.proxy.get(\"move_proxy\").x + this.proxy.get(\"move_proxy\").taille / 2);\n\t\t\tthis.dessin.setAttribute(\"y1\", this.proxy.get(\"move_proxy\").y + this.proxy.get(\"move_proxy\").taille / 2);\n\n\t\t\tthis.dessin.setAttribute(\"x2\", this.proxy.get(\"resize_proxy\").x + this.proxy.get(\"resize_proxy\").taille / 2);\n\t\t\tthis.dessin.setAttribute(\"y2\", this.proxy.get(\"resize_proxy\").y + this.proxy.get(\"resize_proxy\").taille / 2);\n\n\t\t\tthis.x2 += movementX;\n\t\t\tthis.y2 += movementY;\n\t\t}\n\t}, {\n\t\tkey: \"resize\",\n\t\tvalue: function resize(movementX, movementY) {\n\t\t\tthis.proxy.get(\"resize_proxy\").move(movementX, movementY);\n\t\t\tthis.x2 += movementX;\n\t\t\tthis.y2 += movementY;\n\t\t\tthis.dessin.setAttribute(\"x2\", this.proxy.get(\"resize_proxy\").x + this.proxy.get(\"resize_proxy\").taille / 2);\n\t\t\tthis.dessin.setAttribute(\"y2\", this.proxy.get(\"resize_proxy\").y + this.proxy.get(\"resize_proxy\").taille / 2);\n\t\t}\n\t}]);\n\n\treturn Ligne;\n}(Forme);\n\nvar Formes = function () {\n\tfunction Formes() {\n\t\t_classCallCheck(this, Formes);\n\n\t\tthis.nbformes = 0;\n\t\tthis.liste = new Map();\n\t}\n\n\t_createClass(Formes, [{\n\t\tkey: \"add\",\n\t\tvalue: function add(forme) {\n\t\t\tthis.liste.set(forme.id, forme);\n\t\t\tthis.nbformes++;\n\t\t\tconsole.log(this.liste);\n\t\t\tconsole.log(this.nbformes);\n\t\t}\n\t}, {\n\t\tkey: \"supprimer\",\n\t\tvalue: function supprimer(forme) {\n\t\t\tthis.liste.delete(forme.id);\n\t\t\tthis.nbformes--;\n\t\t}\n\t}]);\n\n\treturn Formes;\n}();\n\n;\n\nexports.Formes = Formes;\nexports.Ligne = Ligne;\nexports.Cercle = Cercle;\nexports.Rectangle = Rectangle;\nexports.Ancre = Ancre;\nexports.Forme = Forme;\n\n//////////////////\n// WEBPACK FOOTER\n// ./scripts/formes.js\n// module id = 1\n// module chunks = 2\n\n//# sourceURL=webpack:///./scripts/formes.js?");

/***/ })
/******/ ]);
});