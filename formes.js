var svg = "http://www.w3.org/2000/svg";

var idunique= (function(){
			var id=0;
			return function(se){return id++;};
		})();

class Ancre{
	constructor(x, y, _id){
		console.log(x + " " +y)
		this._x = x;
		this._y = y;
		this._taille = 10;
		this.id = _id;
		this.dessin = document.createElementNS(svg, "rect");
			this.dessin.setAttribute("id", this.id);
			this.dessin.setAttribute("width", this.taille);
			this.dessin.setAttribute("height", this.taille);
			this.dessin.setAttribute("x", this.x);
			this.dessin.setAttribute("y", this.y);
			this.dessin.setAttribute("fill", "black");
	}
	get x() { return this._x};
	get y() { return this._y};
	get taille() {return this._taille};
	set x(value) {
		this._x=value
		this.dessin.setAttribute("x", this.x);
	};
	set y(value) {
		this._y=value
		this.dessin.setAttribute("y", this.y);
	};
	dessiner(){
		feuille.appendChild(this.dessin);
	}
	cacher(){
		feuille.removeChild(this.dessin)
	}
	move(movementX, movementY){
		this.x += movementX;
		this.y += movementY
	}
};

class Forme{
	constructor(x, y, forme){
		this._id = forme+idunique();

		this.proxy = new Map();
		this.proxy.set("move_proxy", new Ancre(x, y, "move"));

		this._color="transparent";
		this._stroke_color="black";
		this._stroke=3;

		this.dessin = document.createElementNS(svg, forme);
			this.dessin.setAttribute("id", this.id);
			this.dessin.setAttribute("fill", this._color);
			this.dessin.setAttribute("stroke", this._stroke_color)
			this.dessin.setAttribute("stroke-width", this._stroke);
	}
	get id() {return this._id};
	get color(){return this._color;}
	get stroke_color(){return this._stroke_color;}
	get stroke(){return this._stroke;}

	set stroke(value){
		this._stroke=value;
		this.dessin.setAttribute("stroke-width", this._stroke);
	}
	set color(value){
		this._color=value;
		this.dessin.setAttribute("fill", this._color)
	}
	set stroke_color(value){
		this._stroke_color=value;
		this.dessin.setAttribute("stroke", this._stroke_color)
	}
	dessinerAncres(){
		this.proxy.get("move_proxy").dessiner();
		this.proxy.get("resize_proxy").dessiner();
	}
	cacherAncres(){
		this.proxy.get("move_proxy").cacher();
		this.proxy.get("resize_proxy").cacher();
	}
	supprimer(){
		this.cacherAncres();
		feuille.removeChild(this.dessin);
		delete this.proxy.get("move_proxy");
		delete this.proxy.get("resize_proxy");
		console.log(this);
	}
};


class Rectangle extends Forme{
	constructor(_height=75, _width=100, x=30, y=50){
		super(x, y, "rect");

		this.height = _height;
		this.width = _width;

		this.proxy.set("resize_proxy", new Ancre((x+this.width), (y+this.height), "resize"));
			this.dessin.setAttribute("x", (this.proxy.get("move_proxy").x+this.proxy.get("move_proxy").taille/2));
			this.dessin.setAttribute("y", (this.proxy.get("move_proxy").y+this.proxy.get("move_proxy").taille/2));
			this.dessin.setAttribute("width", this.width);
			this.dessin.setAttribute("height", this.height);
			feuille.appendChild(this.dessin);
		}

		resize(movementX, movementY){
			if(this.height > 5){
				this.height += movementY;
				this.proxy.get("resize_proxy").y += movementY;
				this.dessin.setAttribute("height", this.height)
			}
			else if(this.height <= 5){
				this.height +=1 ;
				this.proxy.get("resize_proxy").y += 1;
			}

			if(this.width > 5){
				this.width += movementX;
				this.proxy.get("resize_proxy").x += movementX;
				this.dessin.setAttribute("width", this.width)
			}
			else if(this.width <= 5){
				this.width +=1 ;
				this.proxy.get("resize_proxy").x += 1;
			}
		}
		move(movementX, movementY){
			this.proxy.get("move_proxy").move(movementX, movementY);
			this.proxy.get("resize_proxy").move(movementX, movementY);

			this.dessin.setAttribute("x", (this.proxy.get("move_proxy").x+this.proxy.get("move_proxy").taille/2));
			this.dessin.setAttribute("y", (this.proxy.get("move_proxy").y+this.proxy.get("move_proxy").taille/2));
		}
};

class Cercle extends Forme{
	constructor(_rayon=50, x=30, y=50){
		super(x, y, "circle");

		this.rayon=_rayon
		this.proxy.set("resize_proxy", new Ancre((x+2*this.rayon+15), (y+2*this.rayon+15), "resize"))
		this.dessin.setAttribute("cx", this.proxy.get("move_proxy").x+this.rayon+15);
		this.dessin.setAttribute("cy", this.proxy.get("move_proxy").y+this.rayon+15);
		this.dessin.setAttribute("r", this.rayon);
		feuille.appendChild(this.dessin);
	}
	move(movementX, movementY){
		this.proxy.get("move_proxy").move(movementX, movementY);
		this.proxy.get("resize_proxy").move(movementX, movementY);

		this.dessin.setAttribute("cx", this.proxy.get("move_proxy").x+this.rayon+15);
		this.dessin.setAttribute("cy", this.proxy.get("move_proxy").y+this.rayon+15);
	}
	resize(movementX, movementY){
		if(this.rayon>1){
			this.proxy.get("resize_proxy").move(movementX, movementY);
			let deplacement = Math.sqrt(Math.pow(movementX, 2) + Math.pow(movementY, 2));
			if(movementX > 0 || movementY > 0){
				this.rayon += deplacement;
			}
			else{
				this.rayon -= deplacement;
			}
			this.dessin.setAttribute("r", this.rayon);
		}
		else if(this.rayon<=1){
			this.rayon += 1;
		}
	}
}

class Ligne extends Forme{
	constructor(x1=50, _x2=150, y1=60, _y2=100){
		super(x1, y1, "line");

		this.x2=_x2;
		this.y2=_y2;
		this.proxy.set("resize_proxy", new Ancre(this.x2, this.y2, "resize"));
		this.dessin.setAttribute("x1", (this.proxy.get("move_proxy").x+this.proxy.get("move_proxy").taille/2));
		this.dessin.setAttribute("y1", (this.proxy.get("move_proxy").y+this.proxy.get("move_proxy").taille/2));
		this.dessin.setAttribute("x2", (this.proxy.get("resize_proxy").x+this.proxy.get("resize_proxy").taille/2));
		this.dessin.setAttribute("y2", (this.proxy.get("resize_proxy").y+this.proxy.get("resize_proxy").taille/2));
		feuille.appendChild(this.dessin);
	}
	move(movementX, movementY){
		this.proxy.get("move_proxy").move(movementX, movementY);
		this.proxy.get("resize_proxy").move(movementX, movementY);

		this.dessin.setAttribute("x1", (this.proxy.get("move_proxy").x+this.proxy.get("move_proxy").taille/2));
		this.dessin.setAttribute("y1", (this.proxy.get("move_proxy").y+this.proxy.get("move_proxy").taille/2));

		this.dessin.setAttribute("x2", (this.proxy.get("resize_proxy").x+this.proxy.get("resize_proxy").taille/2));
		this.dessin.setAttribute("y2", (this.proxy.get("resize_proxy").y+this.proxy.get("resize_proxy").taille/2));

		this.x2 += movementX;
		this.y2 += movementY;
	}
	resize(movementX, movementY){
		this.proxy.get("resize_proxy").move(movementX, movementY);
		this.x2 += movementX;
		this.y2 += movementY;
		this.dessin.setAttribute("x2", (this.proxy.get("resize_proxy").x+this.proxy.get("resize_proxy").taille/2));
		this.dessin.setAttribute("y2", (this.proxy.get("resize_proxy").y+this.proxy.get("resize_proxy").taille/2));
	}
}

class Formes{
	constructor(){
		this.nbformes = 0;
		this.liste = new Map();
	}
	add(forme){
		this.liste.set(forme.id, forme);
		this.nbformes ++;
		console.log(this.liste);
		console.log(this.nbformes);
	}
	supprimer(forme){
		this.liste.delete(forme.id);
		this.nbformes -- ;
	}
};
