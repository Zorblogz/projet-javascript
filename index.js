import { Formes, Ligne, Cercle, Rectangle, Ancre, Forme } from "./formes.js"

var feuille = document.querySelector("#feuille");
document.querySelector("#add_rec").addEventListener("click", draw_rec, false);
document.querySelector("#add_circle").addEventListener("click", draw_circle, false);
document.querySelector("#add_line").addEventListener("click", draw_line, false);

console.log(feuille);
var formes = new Formes();	
var selected_shape;

function getClicked(e)
{
	close_menu();
	if(selected_shape != null){
		selected_shape.cacherAncres();
		selected_shape = null;
	}
	let id = e.target.attributes.id.value;
	selected_shape = formes.liste.get(e.target.attributes.id.value);
	selected_shape.dessinerAncres();
	document.querySelector("#move").addEventListener("mousedown", (e) => { maintain("move", e) }, false);
	document.querySelector("#resize").addEventListener("mousedown", (e) => { maintain("resize", e)}, false);
	feuille.addEventListener("scroll", set_stroke)
	open_menu();
}

function maintain(action, e)
{
	if(action == "move"){
		feuille.addEventListener("mousemove", move_shape, false);				
	}
	else if(action=="resize"){
		feuille.addEventListener("mousemove", resize_shape, false);
	}
	feuille.addEventListener("mouseup", release, false);
}

function move_shape(e)
{
	selected_shape.move(e.movementX, e.movementY);
}

function resize_shape(e)
{
	selected_shape.resize(e.movementX, e.movementY);
}

function release(e)
{
	feuille.removeEventListener("mousemove", move_shape, false);
	feuille.removeEventListener("mousemove", resize_shape, false);
	feuille.removeEventListener("mouseup", release, false); 
}

function open_menu(){
	let menu = document.querySelector("#menu");
	
	menu.innerHTML = '<h3>Modifier : </h3>'
	menu.innerHTML += '<h4> Couleur de la forme : </h4> <input id="fill_color" type="text" placeholder="'+selected_shape.color+'" /> <input id="submit_fill" type="submit"/> </br>'
	menu.innerHTML += '<h4> Couleur de la bordure : </h4> <input id="stroke_color" type="text" placeholder="'+selected_shape.stroke_color+'" /> <input id="submit_stroke_color" type="submit"/> </br>'
	menu.innerHTML += '<h4> Epaisseur de la bordure : </h4> <input id="stroke" type="text" placeholder="'+selected_shape.stroke+'"/> <input id="submit_stroke" type="submit"/> </br>'
	menu.innerHTML += '<input id="delete" type="button" value="SUPPRIMER"/>'	
	document.querySelector("#submit_fill").addEventListener("click", set_color, false);
	document.querySelector("#submit_stroke_color").addEventListener("click", set_stroke_color, false)
	document.querySelector("#submit_stroke").addEventListener("click", set_stroke, false)
	document.querySelector("#delete").addEventListener("click", delete_shape, false)
}

function set_color(e){
	let color = document.querySelector("#fill_color").value;
	selected_shape.color=color;
	console.log(color)
}

function set_stroke_color(e){
	let color = document.querySelector("#stroke_color").value;
	selected_shape.stroke_color=color;
	console.log(color)
}

function set_stroke(e){
	let stroke = document.querySelector("#stroke").value;
	selected_shape.stroke=stroke
}

function delete_shape(e){
	selected_shape.supprimer();
	close_menu();
	formes.supprimer(selected_shape);
	console.log(formes);
	selected_shape=null;
}

function close_menu(){
	let menu = document.querySelector("#menu");
	menu.innerHTML = " "
}

function draw_rec()
{
	let rectangle = new Rectangle();
	formes.add(rectangle);	
	document.querySelector('#'+rectangle.id).addEventListener("click", getClicked, false);
	console.log(rectangle);
}

function draw_circle()
{
	let cercle = new Cercle();
	formes.add(cercle);
	document.querySelector('#'+cercle.id).addEventListener("click", getClicked, false);
	console.log(cercle);
}

function draw_line()
{
	let ligne = new Ligne()
	formes.add(ligne);
	document.querySelector('#'+ligne.id).addEventListener("click", getClicked, false);
	console.log(ligne);
}