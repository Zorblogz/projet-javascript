HTML
======

Ajouter une forme: clic sur "ajouter"+forme souhait�e
Deplacer une forme: clic sur la forme pour selectionner, clic enfonc� sur l'ancre en haut � droite de la forme et deplacer la souris
Redimensionner une forme: clic sur la forme pour selectionner, clic enfonc� sur l'ancre en bas � gauche de la forme et deplacer la souris
Modifier une forme: clic sur la forme pour selectionner, un menu apparait avec des champs � remplir, clic sur valider pour modifier

Javascript
==========

Inclure le fichier formes.js puis module.js

Formes
-------

creer une forme avec new Rectangle, Ligne ou Cercle
creer une instance de Formes, permettant de stocker vos formes avec new Formes

Methodes
---------

###Formes

formes.add(formes) : ajoute une forme dans l'instance
formes.supprimer(forme) : supprime la forme de l'instance 

###Forme

forme.dessinerAncres() permet d'afficher les ancres de la forme
forme.cacherAncres() permet de cacher les ancres de la forme
forme.supprimer() permet de supprimer la forme
forme.move(movementX, movementY) permet de deplacer la forme en utilisant des distances
forme.resize(movementX, movementY) permet de deplacer la forme en utilisant des distances